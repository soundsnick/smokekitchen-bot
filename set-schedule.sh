# This used to save your current cron jobs
crontab -l > tmp_current_cron

# Write in your absolute path to notification scripts
# You can add your own schedule records, using crontab format. Google it.
echo "
20 10 * * * python3 /home/yernazar/www/nefico-bot/notificate.py
30 10 * * * python3 /home/yernazar/www/nefico-bot/notificate-intime.py
50 13 * * * python3 /home/yernazar/www/nefico-bot/notificate.py
00 14 * * * python3 /home/yernazar/www/nefico-bot/notificate-intime.py
20 17 * * * python3 /home/yernazar/www/nefico-bot/notificate.py
30 17 * * * python3 /home/yernazar/www/nefico-bot/notificate-intime.py
" >> tmp_current_cron

# Install new cron file
crontab tmp_current_cron
rm tmp_current_cron
