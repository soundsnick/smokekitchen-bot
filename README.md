# SMOKE KITCHEN NOTIFICATOR
## Description
Smoking time notificator with no listener, and only on crontab.

### Scripts Info
- `notificate.py` is a script for notifications 10 minutes till the event
- `notificate-intime.py` is a script for notification in exact time of the event  

> **_NOTE:_** You configure your schedule using *set-schedule* anyways. Two scripts differ with different content that are stored in *data.py*. You may configure it yourself, if you want. You can add, or remove items. Message or sticker is received randomly from the list.

## Installation and usage
- Copy `config.example.py` into `config.py`
- Copy `data.example.py` into `data.py`
- Configure `set-schedule.sh`, and run it `sh set-schedule.sh`.