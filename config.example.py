ENV_IS_TEST = True # Used for...you know what it's used for

CHAT_ID = 'HERE-IS-YOUR-TEST-CHAT-ID' if ENV_IS_TEST else 'YOUR-PRODUCTION-CHAT-ID'
TOKEN = 'YOUR-BOT-TOKEN'