import requests
from random import randint

from config import CHAT_ID, TOKEN
from data import phrases, mentions
from handlers import sendMessage

# Make a msg send URL
msg = "Через 10 минут Code Review" + mentions

# Dispatch the send request
sendMessage(TOKEN, CHAT_ID, msg)
