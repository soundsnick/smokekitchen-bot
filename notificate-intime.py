import requests
from random import randint

from config import CHAT_ID, TOKEN
from data import phrases_intime, stickers, mentions
from handlers import sendMessage, sendSticker

msg = "CODE REVIEW" + mentions # Make a message send URL

# Dispatch send requests
sendMessage(TOKEN, CHAT_ID, msg)
