import requests

def sendMessage(token, chat_id, text):
    url = 'https://api.telegram.org/bot' + token + '/sendMessage?text=' + text + '&chat_id=' + chat_id
    requests.get(url)

def sendSticker(token, chat_id, file_id):
    url = 'https://api.telegram.org/bot' + token + '/sendSticker?sticker=' + file_id + '&chat_id=' + chat_id
    requests.get(url)
