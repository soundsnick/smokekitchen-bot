import datetime

time = datetime.datetime.now()
current_time = str(time.hour) + ':' + str(time.minute)


mentionsArr = ['@yarnduffold', '@sdanabek', '@adayzhumabek', '@YNastasiya']
mentions = '\n'+(" ").join(mentionsArr)

phrases = []

phrases_intime = []

stickers = [
    'CAACAgIAAxkBAAMGYKc_LYlbWT2pvDrNNdfG8-dJlP0AAtMBAAJ0-FQbnzckccv_dswfBA',
    'CAACAgIAAxkBAAMIYKc_WeOQUOe0GQsAAbsCSiqF5C1uAAJQAwACsH6FDjmk-Rz-b0yDHwQ',
    'CAACAgIAAxkBAAMJYKc_fn2tsr4Q1WgOBnOsT1q31jAAAjgDAAK1cdoGwvLeWs_qDRUfBA',
    'CAACAgQAAxkBAAMKYKc_lgfqGTNbK6WhlyUAARwzWH4UAAI0AQACa65eCT3VZgaKypO9HwQ',
    'CAACAgIAAxkBAAMLYKc_ry7H62O4-JZ_VTxoMgSFNQwAAtcGAAJjK-IJKqE0aQsVNeUfBA',
    'CAACAgIAAxkBAAMMYKc_yVVLPzU0ZfZFAkZec1uiGmoAAv8AA7rAlQVTNI2eZ9wNdx8E',
    'CAACAgIAAxkBAAMNYKc_5COgFEzUzF2Kn9WO-eBAKuUAAl0AAw56-wqppMkdSp07Ex8E',
    'CgACAgQAAxkBAAMOYKdAG_73Ew0GAAHfjA8eWPfg4w4VAAKfAgACjTyVUiUlUmNjcgdKHwQ',
    'CAACAgIAAxkBAAMQYKdAnbkGAAGIERe5w7GjJHOG4RemAALGAwACnNbnCpvlWJkwtHPSHwQ',
    'CAACAgIAAxkBAAMRYKdAsIkvJHrgMpZCrQOBAAEF9bvUAALHAwACnNbnCuzXmKhgAms6HwQ'
]
