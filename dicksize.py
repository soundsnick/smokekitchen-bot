import requests
from random import randint
from functools import reduce

from config import CHAT_ID, TOKEN
from data import phrases_intime, stickers, mentionsArr
from handlers import sendMessage, sendSticker

def get_emoji_from_size(size):
    res = '🤣🤣🤣'
    if size > 5 and size <= 10:
        res = '😣' 
    if size > 10 and size <= 15:
        res = '😏👌🏻' 
    if size > 15 and size <= 20:
        res = '😌' 
    if size > 20 and size <= 30:
        res = '🤪' 
    if size > 30 and size <= 40:
        res = '😨' 
    if size > 40:
        res = '🤯' 
    return res

def gen_dick_size():
    size = randint(0, 50)
    return str(size) + "cm today " + get_emoji_from_size(size)

msg = reduce(
    lambda acc, n: acc + n + " has got a dick of size " + gen_dick_size() + "\n", 
    mentionsArr, 
    "DICK SIZE TABLE:\n_____________\n"
)

# Dispatch send requests
sendMessage(TOKEN, CHAT_ID, msg)